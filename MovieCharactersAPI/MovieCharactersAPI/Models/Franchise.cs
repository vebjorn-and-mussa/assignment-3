﻿using System.ComponentModel.DataAnnotations;
namespace MovieCharactersAPI.Models;
public class Franchise
{
    [Key]
    public int Id { get; set; }
    [MaxLength(64)]
    public string Name { get; set; }
    [MaxLength(1024)]
    public string Description { get; set; }

    // navigation properties
    public ICollection<Movie> Movies { get; set; }
}