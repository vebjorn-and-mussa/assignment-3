﻿using System.ComponentModel.DataAnnotations;

namespace MovieCharactersAPI.Models;
public class Movie
{
    [Key]
    public int Id { get; set; }
    [MaxLength(64)]
    public string Title { get; set; }
    [MaxLength(256)]
    public string Genre { get; set; }
    public int ReleaseYear { get; set; }
    [MaxLength(64)]
    public string Director { get; set; }
    [MaxLength(256)]
    public string? Picture { get; set; }
    [MaxLength(256)]
    public string? Trailer { get; set; }

    // navigaton properties
    public ICollection<Character> Characters { get; set; }
    public int? FranchiseId {get; set; }
    public Franchise? Franchise { get; set; }
}