﻿using MovieCharactersAPI.Models;

namespace MovieCharactersAPI.Interfaces
{
    public interface ICharacterRepository : IRepository<Character>
    {

    }
}
