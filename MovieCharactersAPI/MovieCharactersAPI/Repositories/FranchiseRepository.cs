﻿using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Data;
using MovieCharactersAPI.Interfaces;
using MovieCharactersAPI.Models;

namespace MovieCharactersAPI.Repositories
{
    public class FranchiseRepository : IFranchiseRepository
    {
        private readonly MovieCharactersDbContext _context;

        public FranchiseRepository(MovieCharactersDbContext context)
        {
            _ = context.Franchises ?? throw new ArgumentNullException("context.Franchises was null for some reason?");
            _context = context;
        }

        public async Task<Franchise> Add(Franchise item)
        {
            _context.Franchises.Add(item);
            await _context.SaveChangesAsync();

            return item;
        }

        public async Task<bool> Delete(int id)
        {
            var franchise = await GetById(id);//_context.Characters.FindAsync(id);
            if (franchise == null)
            {
                return false;
            }

            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();

            return true;

        }

        public bool Exist(int id)
        {
            return _context.Franchises.Any(f => f.Id == id);
        }

        public Task<List<Franchise>> GetAll()
        {
            return _context.Franchises.ToListAsync();
        }

        public async Task<Franchise?> GetById(int id)
        {
            return await _context.Franchises.FindAsync(id);
        }

        public async Task<ICollection<Character>> GetCharacters(int franchiseId)
        {
            var characters = new HashSet<Character>();
            var movies = await _context.Movies.Include(m => m.Characters).Where(m => m.FranchiseId == franchiseId).ToListAsync();

            foreach(var movie in movies)
            {
                foreach (var character in movie.Characters)
                {
                    characters.Add(character);
                }
            }
            return characters;
        }

        public async Task<List<Movie>> GetMovies(int franchiseId)
        {
            var franchise = await _context.Franchises.Include(f => f.Movies).FirstOrDefaultAsync(f => f.Id == franchiseId);
            if(franchise is null)
                return new List<Movie>();
            return franchise.Movies.ToList(); 
        }

        public async Task<Franchise?> Update(Franchise item)
        {
            _context.Entry(item).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return item;

        }

        public async Task<Franchise?> UpdateMovies(int id, int[] movieIds)
        {
            var movies = await _context.Movies.Where(m => movieIds.Contains(m.Id)).ToListAsync();

            var franchise = await _context.Franchises.FindAsync(id);
            if (franchise == null)
            {
                return null;
            }

            foreach (var movie in movies)
            {
                movie.Franchise = franchise;
            }

            await _context.SaveChangesAsync();
            return franchise;
        }
    }
}
