﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Data;
using MovieCharactersAPI.DTOs;
using MovieCharactersAPI.Interfaces;
using MovieCharactersAPI.Models;

namespace MovieCharactersAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class MoviesController : ControllerBase
    {
        private readonly IMovieRepository _movieRepository;
        private readonly IMapper _mapper;

        public MoviesController( IMapper mapper, IMovieRepository movieRepository)
        {
            _mapper = mapper;
            _movieRepository = movieRepository;
        }

        // GET: api/Movies
        /// <summary>
        /// Get all movies
        /// </summary>
        /// <returns> A list of movies </returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<MovieReadDto>>> GetMovies()
        {
          var movies = await _movieRepository.GetAll();
          return Ok(movies.Select(movie => _mapper.Map<MovieReadDto>(movie)));
        }

        /// <summary>
        /// Get a movie by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns> a movie </returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<MovieReadDto>> GetMovie(int id)
        {
            var movie = await _movieRepository.GetById(id);

            if (movie == null)
            {
                return NotFound();
            }

            return _mapper.Map<MovieReadDto>(movie);
        }

        // PUT: api/Movies/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Updates a movie by using provided movie and Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movieDto"></param>
        /// <returns>void</returns>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<IActionResult> PutMovie(int id, MovieUpdateDto movieDto)
        {
            var movie = _mapper.Map<Movie>(movieDto);
            if(id != movie.Id)
                return BadRequest();
            movie = await _movieRepository.Update(movie);

            if(movie is null)
                return NotFound();
            return NoContent();
        }

        // POST: api/Movies
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Post a movie to the database
        /// </summary>
        /// <param name="movieDto"></param>
        /// <returns>the uploaded movie</returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<MovieReadDto>> PostMovie(MovieCreateDto movieDto)
        {
            var movie = _mapper.Map<Movie>(movieDto);

            movie = await _movieRepository.Add(movie);

            return CreatedAtAction("GetMovie", new { id = movie.Id }, _mapper.Map<MovieReadDto>(movie));
        }

        // DELETE: api/Movies/5
        /// <summary>
        /// Deletes a movie
        /// </summary>
        /// <param name="id"></param>
        /// <returns>void</returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            var success = await _movieRepository.Delete(id);

            if(!success)
                return NotFound();

            return NoContent();
        }

        /// <summary>
        /// Sets a list of characters, given my Id, to a movie
        /// </summary>
        /// <param name="movieId"></param>
        /// <param name="characterIds"></param>
        /// <returns>The movie the characters where uploaded to</returns>
        [HttpPost("updateCharacters/{movieId}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> UpdateCharacters(int movieId, int[] characterIds)
        {
            if(!MovieExists(movieId))
                return NotFound();

            var characters = await _movieRepository.UpdateCharacters(movieId, characterIds);

            return CreatedAtAction(nameof(GetCharacters), new { id=movieId }, characters.Select(c => _mapper.Map<CharacterReadDto>(c)));
        }

        /// <summary>
        /// Returns a list of all characters in speificed movie, by Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A list of characters</returns>
        [HttpGet("GetCharacters/{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<CharacterReadDto>>> GetCharacters(int id)
        {
            if(!MovieExists(id))
                return NoContent();
            var characters = await _movieRepository.GetCharacters(id);

            return Ok(characters.Select(c => _mapper.Map<CharacterReadDto>(c)));
        }


        private bool MovieExists(int id)
        {
            return _movieRepository.Exist(id);
        }
    }
}
