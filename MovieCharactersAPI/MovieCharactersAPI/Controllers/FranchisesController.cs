﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Data;
using MovieCharactersAPI.DTOs;
using MovieCharactersAPI.Interfaces;
using MovieCharactersAPI.Models;

namespace MovieCharactersAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class FranchisesController : ControllerBase
    {
        private readonly IFranchiseRepository _franchiseRepository;
        private readonly IMapper _mapper;

        public FranchisesController(IMapper mapper, IFranchiseRepository franchiseRepository)
        {
            _mapper = mapper;
            _franchiseRepository = franchiseRepository;
        }

        /// <summary>
        /// It will show all franchises
        /// </summary>
        /// <returns>List of franchises</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDto>>> GetFranchises()
        {
            var franchises = await _franchiseRepository.GetAll();
            return Ok(franchises.Select(f => _mapper.Map<FranchiseReadDto>(f)));
        }

        /// <summary>
        /// A franchise, given by Id.
        /// </summary>
        /// <param name="id">Franchise Id</param>
        /// <returns>Franchise</returns>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDto>> GetFranchise(int id)
        {
            var franchise = await _franchiseRepository.GetById(id);

            if (franchise == null)
            {
                return NotFound();
            }

            return _mapper.Map<FranchiseReadDto>(franchise);
        }

        /// <summary>
        /// Updates an existing franchise, given by Id
        /// </summary>
        /// <param name="id">Franchise Id</param>
        /// <param name="franchiseDTO">new values</param>
        /// <returns>An updated franchise</returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, FranchiseUpdateDto franchiseDTO)
        {
            var franchise = _mapper.Map<Franchise>(franchiseDTO);
            if(id != franchise.Id)
                return BadRequest();

            franchise = await _franchiseRepository.Update(franchise);
            if(franchise == null)
                return NotFound();
            return NoContent();
        }

        // POST: api/Franchises
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Register a Franchise
        /// </summary>
        /// <param name="franchiseDTO"></param>
        /// <returns>Franchise tha was created</returns>
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpPost]
        public async Task<ActionResult<FranchiseReadDto>> PostFranchise(FranchiseCreateDto franchiseDTO)
        {
            var franchise = _mapper.Map<Franchise>(franchiseDTO);

            franchise = await _franchiseRepository.Add(franchise);

            return CreatedAtAction("GetFranchise", new { id = franchise.Id }, _mapper.Map<FranchiseReadDto>(franchise));
        }

        /// <summary>
        /// Deletes a specific franchise.
        /// </summary>
        /// <param name="id">Franchise Id.</param>
        /// <returns>Franchise that was deleted</returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            var success = await _franchiseRepository.Delete(id);
            if(!success)
                return NotFound();

            return NoContent();
        }

        /// <summary>
        /// Sets a list of movies, given by Id, to a franchise. 
        /// </summary>
        /// <param name="franchiseId">Franchise Id.</param>
        /// <param name="movieIds">List of movie Ids</param>
        /// <returns>Movies added to a Franchise</returns>
        [HttpPut("updateMovies/{franchiseId}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> UpdateMovies(int franchiseId, int[] movieIds)
        {
            var franchise = await _franchiseRepository.UpdateMovies(franchiseId, movieIds);
            if (franchise is null)
            {
                return NotFound();
            }
            return CreatedAtAction("GetFranchise", new { id = franchise.Id }, _mapper.Map<FranchiseReadDto>(franchise));
        }

        /// <summary>
        /// List of movies tha belong to a speific franchise
        /// </summary>
        /// <param name="id">Franchise Id</param>
        /// <returns>All the movies that belongs to the chosen franchise</returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet("GetMovies/{id}")]
        public async Task<ActionResult<IEnumerable<MovieReadDto>>> GetMovies(int id)
        {
            if(!FranchiseExists(id))
            {
                return NotFound();
            }
            var movies = await _franchiseRepository.GetMovies(id);
            if (movies.Count == 0)
            {
                return NoContent();
            }

            return Ok(movies.Select(movie => _mapper.Map<MovieReadDto>(movie)));
        }

        /// <summary>
        /// Get all characters that are associated to a specific Franchise, given by Id.
        /// In order to get Characters that belong to the franchise, is it needed to have movie table in order to associate characters and franchise.
        /// </summary>
        /// <param name="id">Franchise Id</param>
        /// <returns>All characters that belong to that Franchise</returns>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet("GetCharacters/{id}")]
        public async Task<ActionResult<IEnumerable<CharacterReadDto>>> GetCharacters(int id)
        {
            if (!FranchiseExists(id))
            {
                return NotFound();
            }

            var characters = await _franchiseRepository.GetCharacters(id);

            return Ok(characters.Select(c => _mapper.Map<CharacterReadDto>(c)));
        }


        private bool FranchiseExists(int id)
        {
            return _franchiseRepository.Exist(id);
        }
    }
}
