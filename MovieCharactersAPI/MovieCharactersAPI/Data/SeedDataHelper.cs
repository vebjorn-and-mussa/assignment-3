﻿using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Models;

namespace MovieCharactersAPI.Data;
public class SeedDataHelper
{
    public static List<Franchise> GetFranchises()
    {
        List<Franchise> franchises = new List<Franchise>()
            {
                new Franchise() {Id = 1, Name = "Marvel Movies", Description="The Marvel Cinematic Universe (MCU) films are a series of American superhero films produced by Marvel Studios based on characters that appear in publications by Marvel Comics. The MCU is the shared universe in which all of the films are set. The films have been in production since 2007, and in that time Marvel Studios has produced and released 29 films, with at least 14 more in various stages of development. It is the highest-grossing film franchise of all time, having grossed over $27.4 billion at the global box office. This includes Avengers: Endgame, which became the highest-grossing film of all time at the time of its release. "},
                new Franchise() {Id = 2, Name = "Lord of the Rings franchise", Description="The Lord of the Rings is an epic high-fantasy novel by English author and scholar J. R. R. Tolkien. Set in Middle-earth, intended to be Earth at some time in the distant past, the story began as a sequel to Tolkien's 1937 children's book The Hobbit, but eventually developed into a much larger work. Written in stages between 1937 and 1949, The Lord of the Rings is one of the best-selling books ever written, with over 150 million copies sold. "}
            };
        return franchises;
    }

    public static List<Movie> GetMovies()
    {
        List<Movie> movies = new List<Movie>()
            {
                new Movie()
                {
                    Id=1,
                    Title="The Avengers",
                    Genre="Action,Adventure,Sci-Fi",
                    ReleaseYear=2009,
                    Director="Joss Whedon",
                    Picture=@"https://www.imdb.com/title/tt0848228/mediaviewer/rm3955117056/?ref_=tt_ov_i",
                    Trailer=@"https://www.imdb.com/video/vi1891149081?playlistId=tt0848228&ref_=tt_ov_vi",
                    FranchiseId=1,
                },
                new Movie()
                {
                    Id=2,
                    Title="Avengers: Age of Ultron",
                    Genre="Action,Advendture,Sci-Fi",
                    ReleaseYear=2015,
                    Director="Joss Whedon",
                    Picture=@"https://www.imdb.com/title/tt2395427/mediaviewer/rm4050576640/?ref_=tt_ov_i",
                    Trailer=@"https://www.imdb.com/video/vi2821566745?playlistId=tt2395427&ref_=tt_pr_ov_vi",
                    FranchiseId=1,
                },
                new Movie()
                {
                    Id=3,
                    Title="Captain America: Civil War",
                    Genre="Action,Adventure,Sci-Fi",
                    ReleaseYear=2016,
                    Director="Anthony Russo",
                    Picture=@"https://www.imdb.com/title/tt3498820/mediaviewer/rm3218348288/?ref_=tt_ov_i",
                    Trailer=@"https://www.imdb.com/video/vi174044441?playlistId=tt3498820&ref_=tt_ov_vi",
                    FranchiseId=1,
                },
                new Movie()
                {
                    Id=4,
                    Title="Black Panther",
                    Genre="Action,Adventure,Sci-Fi",
                    ReleaseYear=2018,
                    Director="Ryan Coogler",
                    Picture=@"https://www.imdb.com/title/tt1825683/mediaviewer/rm172972800/?ref_=tt_ov_i",
                    Trailer=@"https://www.imdb.com/video/vi2320939289?playlistId=tt1825683&ref_=tt_ov_vi" ,
                    FranchiseId=1,
                },
                new Movie()
                {
                    Id=5,
                    Title="Thor: Ragnarok",
                    Genre="Action,Adventure,Sci-Fi",
                    ReleaseYear=2017,
                    Director="Taika Waititi",
                    Picture=@"https://www.imdb.com/title/tt3501632/mediaviewer/rm1413491712/?ref_=tt_ov_i",
                    Trailer=@"https://www.imdb.com/video/vi4010391833?playlistId=tt3501632&ref_=tt_ov_vi",
                    FranchiseId=1,
                },
                new Movie()
                {
                    Id=6,
                    Title="The Lord of the Rings: The Fellowship of the Ring",
                    Genre="Action,Adventure,Drama,Fantasy",
                    ReleaseYear=2001,
                    Director="Peter Jackson",
                    Picture=@"https://www.imdb.com/title/tt0120737/mediaviewer/rm3592958976/?ref_=tt_ov_i",
                    Trailer=@"https://www.imdb.com/video/vi684573465?playlistId=tt0120737&ref_=tt_ov_vi",
                    FranchiseId=2,
                },
                new Movie()
                {
                    Id=7,
                    Title="The Lord of the Rings: The Two Towers",
                    Genre="Action,Adventure,Drama,Fantasy",
                    ReleaseYear=2002,
                    Director="Peter Jackson",
                    Picture=@"https://www.imdb.com/title/tt0167261/mediaviewer/rm306845440/?ref_=tt_ov_i",
                    Trailer=@"https://www.imdb.com/video/vi701350681?playlistId=tt0167261&ref_=tt_ov_vi",
                    FranchiseId=2,
                },
                new Movie()
                {
                    Id=8,
                    Title="The Lord of the Rings: The Return of the King",
                    Genre="Action,Adventure,Drama,Fantasy",
                    ReleaseYear=2003,
                    Director="Peter Jackson",
                    Picture=@"https://www.imdb.com/title/tt0167260/mediaviewer/rm584928512/?ref_=tt_ov_i",
                    Trailer=@"https://www.imdb.com/video/vi718127897?playlistId=tt0167260&ref_=tt_ov_vi",
                    FranchiseId=2,
                },
            };

        return movies;
    }

    public static List<Character> GetCharacters()
    {
        List<Character> characters = new List<Character>()
            {
                new Character()
                {
                    Id=1,
                    FullName="Tony Stark",
                    Alias="Iron Man",
                    Gender="Male",
                },
                new Character()
                {
                    Id=2,
                    FullName="Steve Rogers",
                    Alias="Captein America",
                    Gender="Male",
                },
                new Character()
                {
                    Id=3,
                    FullName="Natasha Romanoff",
                    Alias="Black Widow",
                    Gender="Female",
                },
                new Character()
                {
                    Id=4,
                    FullName="Thor",
                    Gender="Male",
                },
                new Character()
                {
                    Id=5,
                    FullName="Frodo",
                    Gender="Male",
                },
                new Character()
                {
                    Id=6,
                    FullName="Gandalf",
                    Gender="Male",
                },
                new Character()
                {
                    Id=7,
                    FullName="Pippin",
                    Gender="Male",
                },
                new Character()
                {
                    Id=8,
                    FullName="Galadriel",
                    Gender="Female",
                },
            };
        return characters;
    }

    public static void SeedM2MTableCharacterMovie(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Character>()
            .HasMany(character => character.Movies)
            .WithMany(movie => movie.Characters)
            .UsingEntity<Dictionary<string, object>>
                (
                    "CharacterMovie",
                    right => right.HasOne<Movie>().WithMany().HasForeignKey("MoviesId"),
                    left  => left.HasOne<Character>().WithMany().HasForeignKey("CharactersId"),
                    characterMovie => 
                    {
                        characterMovie.HasKey("MoviesId", "CharactersId");
                        characterMovie.HasData
                        (
                            // The Avengers
                            new { CharactersId = 1, MoviesId = 1 },
                            new { CharactersId = 2, MoviesId = 1 },
                            new { CharactersId = 3, MoviesId = 1 },
                            new { CharactersId = 4, MoviesId = 1 },
                            // The Avengers: Age of ultron
                            new { CharactersId = 1, MoviesId = 2 },
                            new { CharactersId = 2, MoviesId = 2 },
                            new { CharactersId = 3, MoviesId = 2 },
                            new { CharactersId = 4, MoviesId = 2 },
                            // Captain America: Civil war
                            new { CharactersId = 1, MoviesId = 3 },
                            new { CharactersId = 2, MoviesId = 3 },
                            new { CharactersId = 3, MoviesId = 3 },
                            new { CharactersId = 4, MoviesId = 3 },
                            // Black Panther
                            
                            // Thor Ragnarok
                            new { CharactersId = 4, MoviesId = 4 },
                            // Lord of the Rings: The fellowship of the Rings
                            new { CharactersId = 5, MoviesId = 6 },
                            new { CharactersId = 6, MoviesId = 6 },
                            new { CharactersId = 7, MoviesId = 6 },
                            new { CharactersId = 8, MoviesId = 6 },
                            // Lord of the rings: The Two Towers
                            new { CharactersId = 5, MoviesId = 7 },
                            new { CharactersId = 6, MoviesId = 7 },
                            new { CharactersId = 7, MoviesId = 7 },
                            new { CharactersId = 8, MoviesId = 7 },
                            // Lord of the rings: Return of the king
                            new { CharactersId = 5, MoviesId = 8 },
                            new { CharactersId = 6, MoviesId = 8 },
                            new { CharactersId = 7, MoviesId = 8 },
                            new { CharactersId = 8, MoviesId = 8 }
                        );
                    }
                );
    }
}