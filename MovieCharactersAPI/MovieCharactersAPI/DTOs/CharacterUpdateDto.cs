using System.ComponentModel.DataAnnotations;

namespace MovieCharactersAPI.DTOs
{
    public class CharacterUpdateDto
    {
        public int Id { get; set; }
        [StringLength(64)] public string FullName { get; set; }
        [StringLength(64)] public string? Alias { get; set; }
        [StringLength(32)] public string Gender { get; set; }
        [Url, StringLength(256)] public string? Picture { get; set; }

    }
}
