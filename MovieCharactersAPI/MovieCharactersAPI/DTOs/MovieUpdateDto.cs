using System.ComponentModel.DataAnnotations;

namespace MovieCharactersAPI.DTOs
{
    public class MovieUpdateDto
    {
        public int Id { get; set; }
        [StringLength(64)] public string Title { get; set; }
        [StringLength(256)] public string Genre { get; set; }
        public int ReleaseYear { get; set; }
        [StringLength(64)] public string Director { get; set; }
        [Url, StringLength(256)] public string? Picture { get; set; }
        [Url, StringLength(256)] public string? Trailer { get; set; }
    }
}
